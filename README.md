# PURPOSE #

Collaborative editing of sf167.wordpress.com


### Contribution guidelines ###

* using your bitbucket account, clone this repo to your account or request user access to this repo
* Edit the corresponding document online on github.com
* If you think this change should appear on sf167.wordpress.com, submit a pull request

### Who do I talk to? ###

* midhunanew@gmail.com
* jeenaav137@gmail.com